package cz.java.vse.draa06.adventuranacviceni.main;

/**
 * Interface pro sledování hráče a aktualizaci prostorů ve kterých se nachází
 */
public interface Pozorovatel {

    void update();

    /*
    void updateBatoh();

     */

}
