package cz.java.vse.draa06.adventuranacviceni.logika;

/**
 *  Třída PrikazKonec implementuje pro hru příkaz konec.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015

 *  
 */

public class PrikazKonec implements IPrikaz {
    private static final String NAZEV = "konec";
    private Hra hra;

    public PrikazKonec(Hra hra) {
        this.hra = hra;
    }

    public String provedPrikaz(String... parametry) {
        if (parametry.length > 0) {
            return "Ukončit co? Nechápu, proč jste zadal druhé slovo.";
        } else {
            this.hra.setKonecHry(true);
            return "hra ukončena příkazem konec";
        }
    }

    public String getNazev() {
        return "konec";
    }
}
