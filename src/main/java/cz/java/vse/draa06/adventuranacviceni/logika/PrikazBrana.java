package cz.java.vse.draa06.adventuranacviceni.logika;

import java.util.Collection;
import java.util.Iterator;

public class PrikazBrana implements IPrikaz {
    private static final String NAZEV = "znicit_branu";
    private HerniPlan plan;

    public PrikazBrana(HerniPlan plan) {
        this.plan = plan;
    }

    public String getNazev() {
        return "znicit_branu";
    }

    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co chcete zničit?";
        } else {
            String lektvar = parametry[0];
            Batoh kosik = this.plan.getKosik();
            Prostor prostor = this.plan.getAktualniProstor();
            if (kosik.vKosiku("Lektvar_sily")) {
                Collection<Prostor> okoli = this.plan.getAktualniProstor().getVychody();
                Iterator var6 = okoli.iterator();

                while(var6.hasNext()) {
                    Prostor i = (Prostor)var6.next();
                    if (i.jeZnicitelna()) {
                        i.setZnicitelna(false);
                        this.plan.setVyhra(true);
                    }
                }

                return "Zničili jste hlavní bránu";
            } else {
                return "Nemáš lektvar síly, nebo se snažíš probourat bránu / celu něčím jiným!";
            }
        }
    }
}
