package cz.java.vse.draa06.adventuranacviceni.main;

import cz.java.vse.draa06.adventuranacviceni.logika.Batoh;
import cz.java.vse.draa06.adventuranacviceni.logika.HerniPlan;
import cz.java.vse.draa06.adventuranacviceni.logika.Hra;
import cz.java.vse.draa06.adventuranacviceni.logika.Prostor;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;

public class MainController implements Pozorovatel{
    @FXML
    public TextField vstup;
    @FXML
    public TextArea vystup;
    @FXML
    public Button odesli;
    @FXML
    public Button hod;
    @FXML
    public Button odekmnout;
    @FXML
    public Button zniceni;
    @FXML
    public ListView vychody;
    @FXML
    public ImageView hrac;
    @FXML
    public ListView veci;
    @FXML
    public MenuItem napoveda;
    @FXML
    public MenuItem novahra;
    @FXML
    public ImageView kulicky;
    @FXML
    public ImageView klic;
    @FXML
    public ImageView lektvar;
    @FXML
    public MenuButton seberMenu;


    private Hra hra;

    private HerniPlan plan;

    private Batoh kosik;

    private Map<String, Point2D> souradniceProstoru = new HashMap<>();

    /**
     * kontstruktor bez parametrů bude sloužit k vytvoření hry
     */
    public MainController() {
        this.hra = new Hra();
        this.plan = new HerniPlan();
        this.kosik = new Batoh();

        kosik = hra.getHerniPlan().getKosik();

        souradniceProstoru.put("Tvoje_cela", new Point2D(23,77));
        souradniceProstoru.put("Honzova_cela", new Point2D(146,77));
        souradniceProstoru.put("Propojovaci_chodba", new Point2D(82,193));
        souradniceProstoru.put("Prazdna_mistnost", new Point2D(80,331));
        souradniceProstoru.put("Propojovaci_hala", new Point2D(262,193));
        souradniceProstoru.put("Kuchyn", new Point2D(263,331));
        souradniceProstoru.put("Mistnost_pro_psy", new Point2D(263,77));
        souradniceProstoru.put("Zbrojirna", new Point2D(379,193));
        souradniceProstoru.put("Mistnost_pro_straze", new Point2D(378,77));
        //souradniceProstoru.put("Tajna_mistnost", new Point2D(494,77));
        souradniceProstoru.put("Hlavni_brana", new Point2D(494,193));
    }

    /**
     * metoda initialize se injektuje z FXML
     * spouští se po načtení FXML, spolu se všemi komponentami
     * volá se logicky až po konstruktoru
     */
    @FXML
    public void initialize() {
        //zamezení vstupu do pole
        vystup.setEditable(false);

        //vrácení uvítání a zapsání do výstupního pole
        vystup.appendText(hra.vratUvitani()+"\n\n");

        hra.getHerniPlan().registrace(this);
        //hra.getHerniPlan().registraceBatoh(this);
        update();
        //updateBatoh();

        kulicky.setVisible(false);
        klic.setVisible(false);
        lektvar.setVisible(false);

        //kosik = hra.getHerniPlan().getKosik();

        if(kosik.vKosiku("Omracujici_kulicky"))
        {
            kulicky.setVisible(true);
        }
        if(kosik.vKosiku("Klic_od_zbrojirny"))
        {
            klic.setVisible(true);
        }
        if(kosik.vKosiku("Lektvar_sily"))
        {
            lektvar.setVisible(true);
        }
    }


    /**
     * metoda načte vstup z TextFieldu a vypíše do výstupního pole TextArea
     * @param actionEvent
     */
    public void zpracujVstup(ActionEvent actionEvent) {
        //načtení vstupu z komponenty TextField
        String prikaz = vstup.getText();

        vstup.setText("");

        odesliPrikaz(prikaz);
    }

    /**
     * metoda odesílá příkaz napsaný v textovém poli
     * @param prikaz
     */
    private void odesliPrikaz(String prikaz) {

        vystup.appendText(prikaz+"\n");

        String vysledek = hra.zpracujPrikaz(prikaz);

        vystup.appendText(vysledek+"\n\n");

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            vychody.setDisable(true);
            napoveda.setDisable(true);
            lektvar.setVisible(false);
            klic.setVisible(false);
            kulicky.setVisible(false);
            hod.setDisable(true);
            odekmnout.setDisable(true);
            zniceni.setDisable(true);
            seberMenu.setDisable(true);
        }
    }

    /**
     * metoda aktualizuje aktuální prostor, kde se nachází hráč
     * dále nastavuje pozici hráče na mapě
     *
     */
    @Override
    public void update() {
        //System.out.println("Pozorovatelný objekt zasílá změnu");
        vychody.getItems().clear();
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        vychody.getItems().addAll(aktualniProstor.getVychody());

        hrac.setX(souradniceProstoru.get(aktualniProstor.getNazev()).getX());
        hrac.setY(souradniceProstoru.get(aktualniProstor.getNazev()).getY());
    }


    /*
    @Override
    public void updateBatoh() {

        veci.getItems().clear();
        Batoh kosik = hra.getHerniPlan().getKosik();
        veci.getItems().addAll( kosik.getObsahKosiku() + "\n");


    }

     */

    /**
     * metoda načítá východy prostoru ve kterém se hráč aktuálně nachází
     * @param mouseEvent
     */
    public void nactiVychod(MouseEvent mouseEvent) {
        Prostor cil = (Prostor) vychody.getSelectionModel().getSelectedItem();
        System.out.println(cil);

        odesliPrikaz("jdi " + cil);

        if(kosik.vKosiku("Omracujici_kulicky"))
        {
            kulicky.setVisible(true);
        }
        if(kosik.vKosiku("Klic_od_zbrojirny"))
        {
            klic.setVisible(true);
        }
        if(kosik.vKosiku("Lektvar_sily"))
        {
            lektvar.setVisible(true);
        }
        //oprava
        //oprava1
    }

    /**
     * metoda pro tlačítko nová hra
     * ukončí stávající progres hráče a spustí se nová hra
     * @param actionEvent
     */
    public void novaHra(ActionEvent actionEvent) {
        hra = new Hra();
       // veci.getItems().clear();
        vystup.setText(hra.vratUvitani()+ "\n");

        hra.getHerniPlan().registrace(this);
        //hra.getHerniPlan().registraceBatoh(this);
        update();

        vstup.setDisable(false);
        odesli.setDisable(false);
        vychody.setDisable(false);
        napoveda.setDisable(false);

        kulicky.setVisible(false);
        klic.setVisible(false);
        lektvar.setVisible(false);

        hod.setDisable(false);
        odekmnout.setDisable(false);
        zniceni.setDisable(false);

        seberMenu.setDisable(false);
    }


    /**
     * metoda pro tlačítko nápověda
     * v novém okně se objeví nápověda ke hře
     * @param actionEvent
     */
    public void napoveda(ActionEvent actionEvent) {
        WebView webView = new WebView();
        webView.getEngine().load(getClass().getResource("/napoveda.html").toExternalForm());
        Stage stage = new Stage();
        stage.setTitle("Nápověda");
        stage.setScene(new Scene(webView, 1300, 650));
        stage.show();
    }

    /**
     * metoda pro tlačítko seber omračující kulicky
     * to samé jako, když hráč napíše do inputu seber Omracujici_kulicky
     * @param actionEvent
     */
    public void onOmracKulicky(ActionEvent actionEvent) {
        String prikaz = "seber Omracujici_kulicky";

        odesliPrikaz(prikaz);

        //Batoh kosik = hra.getHerniPlan().getKosik();
        kosik = hra.getHerniPlan().getKosik();

        if(kosik.vKosiku("Omracujici_kulicky"))
        {
            kulicky.setVisible(true);
        }


    }

    /**
     *  metoda pro tlačítko seber klíč
     *  to samé jako, když hráč napíše do inputu seber Klic_od_zbrojirny
     * @param actionEvent
     */
    public void onKlic(ActionEvent actionEvent) {
        String prikaz = "seber Klic_od_zbrojirny";

        odesliPrikaz(prikaz);

        //Batoh kosik = hra.getHerniPlan().getKosik();
        kosik = hra.getHerniPlan().getKosik();

        if(kosik.vKosiku("Klic_od_zbrojirny"))
        {
            klic.setVisible(true);
        }
    }

    /**
     * metoda pro tlačítko seber lektavr
     * to samé jako, když hráč napíše do inputu seber Lektvar_sily
     * @param actionEvent
     */
    public void onLektvar(ActionEvent actionEvent) {
        String prikaz = "seber Lektvar_sily";

        odesliPrikaz(prikaz);

        //Batoh kosik = hra.getHerniPlan().getKosik();
        kosik = hra.getHerniPlan().getKosik();

        if(kosik.vKosiku("Lektvar_sily"))
        {
            lektvar.setVisible(true);
        }
    }

    /**
     * metoda pro tlačítko hod omračující kulicky
     * to samé jako, když hráč napíše do inputu hod Omracujici_kulicky
     * @param actionEvent
     */
    public void onHodKulicky(ActionEvent actionEvent) {
        String prikaz = "hod Omracujici_kulicky";

        odesliPrikaz(prikaz);

        kulicky.setVisible(false);
    }

    /**
     * metoda pro tlačítko odemknout Zbrojirna
     * to samé jako, když hráč napíše do inputu odemknout Zbrojirna
     * @param actionEvent
     */
    public void onOdemknout(ActionEvent actionEvent) {
        String prikaz = "odemknout Zbrojirna";

        odesliPrikaz(prikaz);

        klic.setVisible(false);
    }

    /**
     * metoda pro tlačítko znicit
     *      *  to samé jako, když hráč napíše do inputu znicit Hlavni_brana nebo Honzova_cela
     * @param actionEvent
     */
    public void onZnicit(ActionEvent actionEvent) {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        String nazev = aktualniProstor.getNazev();
        if(nazev.equals("Zbrojirna"))
        {
            String prikaz = "znicit_branu Hlavni_brana";
            odesliPrikaz(prikaz);
        }
        if(nazev.equals("Propojovaci_chodba"))
        {
            String prikaz = "znicit_celu Honzova_cela";
            odesliPrikaz(prikaz);
        }
    }
}
