package cz.java.vse.draa06.adventuranacviceni.logika;


import cz.java.vse.draa06.adventuranacviceni.main.Pozorovatel;
import cz.java.vse.draa06.adventuranacviceni.main.Pozorovatelny;

import java.util.HashSet;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 *
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */
public class HerniPlan implements Pozorovatelny {
    private Prostor aktualniProstor;
    private Prostor prostorVyhry;
    private Batoh kosik;
    private boolean vyhra = false;
    private boolean vyhraHonza = false;

    //Seznam pozorovatelů změn
    private Set<Pozorovatel> seznamPozorovatelu = new HashSet<>();

    private Set<Pozorovatel> seznamPozorovateluBatoh = new HashSet<>();

    public HerniPlan() {
        this.zalozProstoryHry();
        this.kosik = new Batoh();
    }

    private void zalozProstoryHry() {
        Prostor tvojeCela = new Prostor("Tvoje_cela", "Tvoje cela, kde jsi zavřený");
        Prostor honzovaCela = new Prostor("Honzova_cela", "Cela, kde je zavřený vězeň Honza");
        Prostor propojovaciChodba = new Prostor("Propojovaci_chodba", "Chodba skrz kterou se dostaneš do tvé celi, Honzovi celi a do prázdné místnosti");
        Prostor prazdnaMistnost = new Prostor("Prazdna_mistnost", "Nic zde není, možná támhle na zdi je něco napsáno");
        Prostor propojovaciHala = new Prostor("Propojovaci_hala", "Hala, skrz kterou se dostaneš do propojovací chodby, místnosti pro psy, kuchyně, zbrojírny nebo do místnosti pro stráže");
        Prostor kuchyn = new Prostor("Kuchyn", "Zde se vaří pokrmy pro celé vězení");
        Prostor mistnostProPsy = new Prostor("Mistnost_pro_psy", "Otevřené kotce se psi");
        Prostor zbrojirna = new Prostor("Zbrojirna", "Skrz zbrojírnu se můžeš dostat k hlavní bráně");
        Prostor mistnostProStraze = new Prostor("Mistnost_pro_straze", "Shromaždiště stráží");
        //Prostor tajnaMistnost = new Prostor("Tajna_mistnost", "Tajná místnost se zajímavými předměty");
        Prostor hlavniBrana = new Prostor("Hlavni_brana", "Odsud už je vidět ven na svobodu");
        //tajnaMistnost.setViditelna(false);
        mistnostProPsy.setSmrt(true);
        mistnostProStraze.setSmrt(true);
        mistnostProStraze.setZneskodnitelni(true);
        zbrojirna.setZamceno(true);
        hlavniBrana.setZnicitelna(true);
        honzovaCela.setZnicitelna(true);
        tvojeCela.setVychod(propojovaciChodba);
        honzovaCela.setVychod(propojovaciChodba);
        prazdnaMistnost.setVychod(propojovaciChodba);
        propojovaciChodba.setVychod(tvojeCela);
        propojovaciChodba.setVychod(honzovaCela);
        propojovaciChodba.setVychod(prazdnaMistnost);
        propojovaciChodba.setVychod(propojovaciHala);
        propojovaciHala.setVychod(propojovaciChodba);
        propojovaciHala.setVychod(kuchyn);
        propojovaciHala.setVychod(mistnostProPsy);
        propojovaciHala.setVychod(zbrojirna);
        propojovaciHala.setVychod(mistnostProStraze);
        kuchyn.setVychod(propojovaciHala);
        mistnostProPsy.setVychod(propojovaciHala);
        zbrojirna.setVychod(propojovaciHala);
        zbrojirna.setVychod(hlavniBrana);
        mistnostProStraze.setVychod(propojovaciHala);
        //mistnostProStraze.setVychod(tajnaMistnost);
        //tajnaMistnost.setVychod(mistnostProStraze);
        hlavniBrana.setVychod(zbrojirna);
        Vec kulicky = new Vec("Omracujici_kulicky", true);
        Vec lektvar = new Vec("Lektvar_sily", true);
        Vec klic = new Vec("Klic_od_zbrojirny", true);
        kuchyn.vlozVec(kulicky);
        //tajnaMistnost.vlozVec(klic);
        mistnostProStraze.vlozVec(klic);
        //mistnostProStraze.vlozVec(mapa);
        zbrojirna.vlozVec(lektvar);
        Clovek honza = new Clovek("Honza", "Psst, poradím ti, když mě zachráníš!v kuchyni můžeš najít něco co omráčí stráže a slyšel jsem, že v prázdné místnosti naprotinás je tajná mapa. Zkus nás odsud dostat!");
        Clovek straz = new Clovek("Stráž", "Stát! Co tu chceš?");
        honzovaCela.pridatCloveka(honza);
        mistnostProStraze.pridatCloveka(straz);
        this.aktualniProstor = tvojeCela;
        this.prostorVyhry = hlavniBrana;
    }

    public boolean jeVyhra() {
        return this.aktualniProstor.equals(this.prostorVyhry);
    }

    public Prostor getAktualniProstor() {
        return this.aktualniProstor;
    }

    public Batoh getKosik() {
        return this.kosik;
    }

    public void setAktualniProstor(Prostor prostor) {
        this.aktualniProstor = prostor;
        upozorniPozotovatele();
        //upozorniPozorovateleBatoh();
    }

    public boolean isVyhra() {
        return this.vyhra;
    }

    public void setVyhra(boolean vyhra) {
        this.vyhra = vyhra;
    }

    public boolean isVyhraHonza() {
        return this.vyhraHonza;
    }

    public void setVyhraHonza(boolean vyhraHonza) {
        this.vyhraHonza = vyhraHonza;
    }

    @Override
    public void registrace(Pozorovatel pozorovatel) {
        seznamPozorovatelu.add(pozorovatel);
    }

    @Override
    public void odregistrace(Pozorovatel pozorovatel) {
        seznamPozorovatelu.remove(pozorovatel);
    }

    @Override
    public void upozorniPozotovatele() {
        for(Pozorovatel pozorovatel : seznamPozorovatelu)
        {
            pozorovatel.update();
        }
    }
}
