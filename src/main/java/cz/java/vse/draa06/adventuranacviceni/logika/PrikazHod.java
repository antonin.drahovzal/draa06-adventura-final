package cz.java.vse.draa06.adventuranacviceni.logika;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

import java.util.Collection;
import java.util.Iterator;

public class PrikazHod implements IPrikaz {
    private static final String NAZEV = "hod";
    private HerniPlan plan;
    private Hra hra;

    public PrikazHod(HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.hra = hra;
    }

    public String getNazev() {
        return "hod";
    }

    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co chcete hodit?";
        } else {
            String kulicky = parametry[0];
            Batoh kosik = this.plan.getKosik();
            if (kosik.vKosiku("Omracujici_kulicky")) {
                Collection<Prostor> okoli = this.plan.getAktualniProstor().getVychody();
                Iterator var5 = okoli.iterator();
                while(var5.hasNext()) {
                    Prostor i = (Prostor)var5.next();
                    if (i.jeZneskodnitelni()) {
                        i.setSmrt(false);
                    }
                }

                kosik.odeberVec("Omracujici_kulicky");
                return "Hodili jste ke strážím omračující kuličky a nyní můžete projít dále.";
            } else {
                return "Nemáš u sebe kuličky, nebo si se pokusil hodit něco jiného!";
            }
        }
    }
}
