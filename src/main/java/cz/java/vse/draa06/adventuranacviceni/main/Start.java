/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.java.vse.draa06.adventuranacviceni.main;



import cz.java.vse.draa06.adventuranacviceni.logika.Hra;
import cz.java.vse.draa06.adventuranacviceni.logika.IHra;
import cz.java.vse.draa06.adventuranacviceni.uiText.TextoveRozhrani;
import cz.java.vse.draa06.adventuranacviceni.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
//import logika.*;
//import uiText.TextoveRozhrani;

/*******************************************************************************
 * T&#x159;&iacute;da {@code Start} je hlavn&iacute; t&#x159;&iacute;dou projektu,
 * kter&yacute; ...
 *
 * @author jm&eacute;no autora
 * @version 0.00.000
 */
public class Start extends Application {
    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args) {
        if(args.length>0 && args[0].equals("-text")) {
            IHra hra = new Hra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
        } else { // pokud parametr není text, spustí se v grafickém

            //spuštění grafického okna
            launch();
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/main.fxml"));
        primaryStage.setTitle("Adventura");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();


    }
}
