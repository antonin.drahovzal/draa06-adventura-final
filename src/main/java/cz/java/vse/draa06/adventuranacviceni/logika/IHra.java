/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.java.vse.draa06.adventuranacviceni.logika;





/**
 *  Rozhraní které musí implementovat hra, je na ně navázáno uživatelské rozhraní
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2014/2015
 */
public interface IHra {
    String vratUvitani();

    String vratEpilog();

    boolean konecHry();

    String zpracujPrikaz(String var1);

    HerniPlan getHerniPlan();
}
