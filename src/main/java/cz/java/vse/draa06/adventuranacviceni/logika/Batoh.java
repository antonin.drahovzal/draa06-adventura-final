package cz.java.vse.draa06.adventuranacviceni.logika;

 

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Map;
/**
 *  Trida Batoh 
 *
 *
 *@author     Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */

public class Batoh
{
    private Map<String, Vec> obsahKosiku = new HashMap();
    private static final int KAPACITA = 3;

    public Batoh() {
    }

    public boolean vlozVec(Vec vec) {
        if (vec.jePrenositelna() && this.obsahKosiku.size() < 3) {
            this.obsahKosiku.put(vec.getNazev(), vec);
            return true;
        } else {
            return false;
        }
    }

    public boolean odeberVec(String nazevVeci) {
        if (this.vKosiku(nazevVeci)) {
            this.obsahKosiku.remove(nazevVeci);
            return true;
        } else {
            return false;
        }
    }

    public boolean vKosiku(String nazevVeci) {
        Iterator var2 = this.obsahKosiku.keySet().iterator();

        String i;
        do {
            if (!var2.hasNext()) {
                return false;
            }

            i = (String)var2.next();
        } while(!i.equals(nazevVeci));

        return true;
    }

    public String getObsahKosiku() {
        String obsah = "";

        String i;
        for(Iterator var2 = this.obsahKosiku.keySet().iterator(); var2.hasNext(); obsah = obsah + i + " ") {
            i = (String)var2.next();
        }

        return obsah;
    }

    public Vec dejVec(String nazev) {
        Vec vec = null;
        if (this.obsahKosiku.containsKey(nazev)) {
            vec = (Vec)this.obsahKosiku.get(nazev);
        }

        return vec;
    }
}



