package cz.java.vse.draa06.adventuranacviceni.logika;

 


/**
 *  Třída PrikazSeber implementuje pro hru příkaz seber.
 *@author     Jarmila Pavlickova, Luboš Pavlíček, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015  
 */
class PrikazSeber implements IPrikaz {
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    private Batoh kosik;

    public PrikazSeber(HerniPlan plan) {
        this.plan = plan;
        this.kosik = plan.getKosik();
    }

    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co mám sebrat? Musíš zadat název věci";
        } else {
            String nazevVeci = parametry[0];
            Prostor aktualniProstor = this.plan.getAktualniProstor();
            Vec sbirana = aktualniProstor.odeberVec(nazevVeci);
            if (sbirana == null) {
                return "To tu není!";
            } else if (!sbirana.jePrenositelna()) {
                aktualniProstor.vlozVec(sbirana);
                return "To neuneseš!";
            } else {
                this.kosik.vlozVec(sbirana);
                return "věc zmizí ze hry";
            }
        }
    }

    public String getNazev() {
        return "seber";
    }
}

