package cz.java.vse.draa06.adventuranacviceni.logika;

/**
 *  Třída PrikazNapoveda implementuje pro hru příkaz napoveda.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    z kurzu 4IT101 pro školní rok 2014/2015

 *  
 */
class PrikazNapoveda implements IPrikaz {
    private static final String NAZEV = "napoveda";
    private SeznamPrikazu platnePrikazy;

    public PrikazNapoveda(SeznamPrikazu platnePrikazy) {
        this.platnePrikazy = platnePrikazy;
    }

    public String provedPrikaz(String... parametry) {
        return "Tvým úkolem je dostat se z vězení.\nMusíš prorazit hlavní bránu.\n\nMůžeš zadat tyto příkazy:\n" + this.platnePrikazy.vratNazvyPrikazu();
    }

    public String getNazev() {
        return "napoveda";
    }
}
