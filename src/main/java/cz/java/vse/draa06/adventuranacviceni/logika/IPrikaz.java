package cz.java.vse.draa06.adventuranacviceni.logika;

/**
 *  Třída implementující toto rozhraní bude ve hře zpracovávat jeden konkrétní příkaz.
 *  Toto rozhraní je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova
 *@version    pro školní rok 2014/2015
 *  
 */
interface IPrikaz {

    String provedPrikaz(String... var1);

    String getNazev();
	
}
