package cz.java.vse.draa06.adventuranacviceni.logika;

public class PrikazVKosiku implements IPrikaz {
    private static final String NAZEV = "kosik";
    private HerniPlan plan;

    public PrikazVKosiku(HerniPlan plan) {
        this.plan = plan;
    }

    public String getNazev() {
        return "kosik";
    }

    public String provedPrikaz(String... parametry) {
        Batoh kosik = this.plan.getKosik();
        return "V košíku je:" + kosik.getObsahKosiku();
    }
}