package cz.java.vse.draa06.adventuranacviceni.logika;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/**
 * Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Alena Buchalcevova
 * @version z kurzu 4IT101 pro školní rok 2014/2015
 */
public class Prostor {
    private String nazev;
    private String popis;
    private Set<Prostor> vychody;
    private Map<String, Vec> veci;
    private Map<String, Clovek> lidi;
    private boolean viditelna = true;
    private boolean smrt = false;
    private boolean zamceno = false;
    private boolean zneskodnitelni = false;
    private boolean znicitelna = false;
    private boolean zachranen = false;

    public Prostor(String nazev, String popis) {
        this.nazev = nazev;
        this.popis = popis;
        this.vychody = new HashSet();
        this.veci = new HashMap();
        this.lidi = new HashMap();
    }

    public void setVychod(Prostor vedlejsi) {
        this.vychody.add(vedlejsi);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof Prostor)) {
            return false;
        } else {
            Prostor druhy = (Prostor)o;
            return Objects.equals(this.nazev, druhy.nazev);
        }
    }

    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = Objects.hashCode(this.nazev);
        int result= 37 * vysledek + hashNazvu;
        return result;
    }

    public String getNazev() {
        return this.nazev;
    }

    public boolean vlozVec(Vec neco) {
        if (this.veci.containsKey(neco.getNazev())) {
            return false;
        } else {
            this.veci.put(neco.getNazev(), neco);
            return true;
        }
    }

    public Vec odeberVec(String nazevVeci) {
        return (Vec)this.veci.remove(nazevVeci);
    }

    public String dlouhyPopis() {
        String var10000 = this.popis;
        return "Jsi v mistnosti/prostoru " + var10000 + ".\n" + this.popisVychodu() + "\n" + this.popisVeci() + "\n" + this.popisLidi();
    }

    public String popisVeci() {
        String vracenyText = "věci: ";

        String nazev;
        for(Iterator var2 = this.veci.keySet().iterator(); var2.hasNext(); vracenyText = vracenyText + " " + nazev) {
            nazev = (String)var2.next();
        }

        return vracenyText;
    }

    private String popisVychodu() {
        String vracenyText = "východy: ";
        Iterator var2 = this.vychody.iterator();

        while(var2.hasNext()) {
            Prostor sousedni = (Prostor)var2.next();
            if (sousedni.jeViditelna()) {
                vracenyText = vracenyText + " " + sousedni.getNazev();
            }
        }

        return vracenyText;
    }

    public Prostor vratSousedniProstor(String nazevSouseda) {
        if (nazevSouseda == null) {
            return null;
        } else {
            Iterator var2 = this.vychody.iterator();

            Prostor i;
            do {
                if (!var2.hasNext()) {
                    return null;
                }

                i = (Prostor)var2.next();
            } while(!i.getNazev().equals(nazevSouseda));

            return i;
        }
    }

    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(this.vychody);
    }

    public boolean clovekVMistnosti(String jmeno) {
        return this.lidi.containsKey(jmeno);
    }

    public Clovek vratCloveka(String jmeno) {
        return (Clovek)this.lidi.get(jmeno);
    }

    public void setSmrt(boolean smrt) {
        this.smrt = smrt;
    }

    public boolean jeSmrt() {
        return this.smrt;
    }

    public void setZnicitelna(boolean znicitelna) {
        this.znicitelna = znicitelna;
    }

    public boolean jeZnicitelna() {
        return this.znicitelna;
    }

    public void setZamceno(boolean zamceno) {
        this.zamceno = zamceno;
    }

    public boolean jeZamceno() {
        return this.zamceno;
    }

    public void setViditelna(boolean viditelna) {
        this.viditelna = viditelna;
    }

    public boolean jeViditelna() {
        return this.viditelna;
    }

    public void setZneskodnitelni(boolean zneskodnitelni) {
        this.zneskodnitelni = zneskodnitelni;
    }

    public boolean jeZneskodnitelni() {
        return this.zneskodnitelni;
    }

    public Vec jeVecVMistnosti(String nazev) {
        Vec vec = null;
        Iterator var3 = this.veci.values().iterator();

        while(var3.hasNext()) {
            Vec i = (Vec)var3.next();
            if (i.getNazev().equals(nazev)) {
                vec = i;
                break;
            }
        }

        return vec;
    }

    public void pridatCloveka(Clovek clovek) {
        this.lidi.put(clovek.getJmeno(), clovek);
    }

    public String popisLidi() {
        String popis = "";
        if (this.lidi.isEmpty()) {
            popis = popis + "Nikdo tu není";
        } else {
            popis = popis + "lidé: ";

            String i;
            for(Iterator var2 = this.lidi.keySet().iterator(); var2.hasNext(); popis = popis + i + " ") {
                i = (String)var2.next();
            }
        }

        return popis;
    }

    public boolean jeVProstoru(String nazev) {
        return this.veci.containsKey(nazev);
    }

    @Override
    public String toString()
    {
        return getNazev();
    }
}
