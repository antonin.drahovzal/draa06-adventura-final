package cz.java.vse.draa06.adventuranacviceni.logika;

 

/*******************************************************************************
 * Třída Vec ...
 *
 *@author     Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */
public class Vec
{
	private String nazev;
	private boolean prenositelnost;
	private boolean odemknutelna = false;
	private boolean jizOdemknuta = false;
	private boolean jenProCteni;

	public Vec(String nazev, boolean prenositelnost) {
		this.nazev = nazev;
		this.prenositelnost = prenositelnost;
	}

	public String getNazev() {
		return this.nazev;
	}

	public boolean jePrenositelna() {
		return this.prenositelnost;
	}

	public void setOdemknuto(boolean jizOdemknuta) {
		this.jizOdemknuta = jizOdemknuta;
	}

	public boolean jeOdemknuto() {
		return this.jizOdemknuta;
	}

}

