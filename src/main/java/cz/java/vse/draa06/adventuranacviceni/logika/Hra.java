package cz.java.vse.draa06.adventuranacviceni.logika;

/**
 *  Třída Hra - třída představující logiku adventury.
 * 
 *  Toto je hlavní třída  logiky aplikace.  Tato třída vytváří instanci třídy HerniPlan, která inicializuje mistnosti hry
 *  a vytváří seznam platných příkazů a instance tříd provádějící jednotlivé příkazy.
 *  Vypisuje uvítací a ukončovací text hry.
 *  Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */

public class Hra implements IHra {
    private SeznamPrikazu platnePrikazy = new SeznamPrikazu();
    private HerniPlan herniPlan = new HerniPlan();
    private boolean konecHry = false;

    public Hra() {
        this.platnePrikazy.vlozPrikaz(new PrikazNapoveda(this.platnePrikazy));
        this.platnePrikazy.vlozPrikaz(new PrikazJdi(this.herniPlan, this));
        this.platnePrikazy.vlozPrikaz(new PrikazSeber(this.herniPlan));
        this.platnePrikazy.vlozPrikaz(new PrikazKonec(this));
        this.platnePrikazy.vlozPrikaz(new PrikazOdemknout(this.herniPlan));
        this.platnePrikazy.vlozPrikaz(new PrikazBrana(this.herniPlan));
        this.platnePrikazy.vlozPrikaz(new PrikazHod(this.herniPlan, this));
        //this.platnePrikazy.vlozPrikaz(new PrikazCist(this.herniPlan));
        this.platnePrikazy.vlozPrikaz(new PrikazVKosiku(this.herniPlan));
        this.platnePrikazy.vlozPrikaz(new PrikazCela(this.herniPlan));
    }

    public String vratUvitani() {
        return "Vítejte!\nToto je příběh o uprchlém vězňovi.\nNapište 'nápověda', pokud si nevíte rady, jak hrát dál.\n\n" + this.herniPlan.getAktualniProstor().dlouhyPopis();
    }

    public String vratEpilog() {
        return "Dík, že jste si zahráli.  Ahoj.";
    }

    public boolean konecHry() {
        return this.konecHry;
    }

    public String zpracujPrikaz(String radek) {
        String[] slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String[] parametry = new String[slova.length - 1];

        for(int i = 0; i < parametry.length; ++i) {
            parametry[i] = slova[i + 1];
        }

        String textKVypsani = " .... ";
        if (this.platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = this.platnePrikazy.vratPrikaz(slovoPrikazu);
            textKVypsani = prikaz.provedPrikaz(parametry);
            Prostor prostor = this.herniPlan.getAktualniProstor();
            if (this.herniPlan.isVyhra() && this.herniPlan.jeVyhra()) {
                this.konecHry = true;
                textKVypsani = textKVypsani + "\nvýhra, ale nezachránil jsi Honzu";
            }

            if (this.herniPlan.isVyhraHonza() && this.herniPlan.jeVyhra()) {
                this.konecHry = true;
                textKVypsani = textKVypsani + "\nvýhra, a zachránil jsi Honzu, gratuluji!";
            }
        } else {
            textKVypsani = "Nevím co tím myslíš? Tento příkaz neznám. ";
        }

        return textKVypsani;
    }

    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }

    public HerniPlan getHerniPlan() {
        return this.herniPlan;
    }
    
}

