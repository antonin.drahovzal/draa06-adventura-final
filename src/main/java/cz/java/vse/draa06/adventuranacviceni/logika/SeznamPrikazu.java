package cz.java.vse.draa06.adventuranacviceni.logika;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *  Class SeznamPrikazu - eviduje seznam přípustných příkazů adventury.
 *  Používá se pro rozpoznávání příkazů
 *  a vrácení odkazu na třídu implementující konkrétní příkaz.
 *  Každý nový příkaz (instance implementující rozhraní Prikaz) se
 *  musí do seznamu přidat metodou vlozPrikaz.
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2013/2014
 */
public class SeznamPrikazu {
    private Map<String, IPrikaz> mapaSPrikazy = new HashMap();

    public SeznamPrikazu() {
    }

    public void vlozPrikaz(IPrikaz prikaz) {
        this.mapaSPrikazy.put(prikaz.getNazev(), prikaz);
    }

    public IPrikaz vratPrikaz(String retezec) {
        return this.mapaSPrikazy.containsKey(retezec) ? (IPrikaz)this.mapaSPrikazy.get(retezec) : null;
    }

    public boolean jePlatnyPrikaz(String retezec) {
        return this.mapaSPrikazy.containsKey(retezec);
    }

    public String vratNazvyPrikazu() {
        String seznam = "";

        String slovoPrikazu;
        for(Iterator var2 = this.mapaSPrikazy.keySet().iterator(); var2.hasNext(); seznam = seznam + slovoPrikazu + ", ") {
            slovoPrikazu = (String)var2.next();
        }

        return seznam;
    }
}

