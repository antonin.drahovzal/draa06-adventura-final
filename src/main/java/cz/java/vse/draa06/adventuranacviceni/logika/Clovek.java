package cz.java.vse.draa06.adventuranacviceni.logika;

public class Clovek {
    private String jmeno;
    private String prvniHovor;
    private String druhyHovor;

    public Clovek(String jmeno, String hovor) {
        this.jmeno = jmeno;
        this.prvniHovor = hovor;
    }

    public String getJmeno() {
        return this.jmeno;
    }

    public String getHovor() {
        return this.druhyHovor;
    }
}
