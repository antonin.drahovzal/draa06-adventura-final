package cz.java.vse.draa06.adventuranacviceni.logika;

import java.util.Collection;
import java.util.Iterator;

public class PrikazOdemknout implements IPrikaz{
    private static final String NAZEV = "odemknout";
    private HerniPlan plan;

    public PrikazOdemknout(HerniPlan plan) {
        this.plan = plan;
    }

    public String getNazev() {
        return "odemknout";
    }

    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Čím chcete odemknout?";
        } else {
            String klic = parametry[0];
            Batoh kosik = this.plan.getKosik();
            if (kosik.vKosiku("Klic_od_zbrojirny")) {
                Collection<Prostor> okoli = this.plan.getAktualniProstor().getVychody();
                Iterator var5 = okoli.iterator();

                while(var5.hasNext()) {
                    Prostor i = (Prostor)var5.next();
                    if (i.jeZamceno()) {
                        i.setZamceno(false);
                    }
                }
                kosik.odeberVec("Klic_od_zbrojirny");
                return "Odemkli jste dveře do zbrojírny";
            } else {
                return "Nemáš klíč, nebo si zkusil odemknout něčím jiným než klíčem!";
            }
        }
    }
}
