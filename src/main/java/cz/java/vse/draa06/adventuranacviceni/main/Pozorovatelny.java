package cz.java.vse.draa06.adventuranacviceni.main;

/**
 * Interface pro sledování hráče a aktualizaci prostorů ve kterých se nachází
 */
public interface Pozorovatelny {

   void registrace(Pozorovatel pozorovatel);

   void odregistrace(Pozorovatel pozorovatel);

   void upozorniPozotovatele();

   /*
   void registraceBatoh(Pozorovatel pozorovatel);

   void odregistraceBatoh(Pozorovatel pozorovatel);

   void upozorniPozorovateleBatoh();

    */

}
