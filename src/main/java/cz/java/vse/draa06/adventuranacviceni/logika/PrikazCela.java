package cz.java.vse.draa06.adventuranacviceni.logika;

import java.util.Collection;
import java.util.Iterator;

public class PrikazCela implements IPrikaz {
    private static final String NAZEV = "znicit_celu";
    private HerniPlan plan;

    public PrikazCela(HerniPlan plan) {
        this.plan = plan;
    }

    public String getNazev() {
        return "znicit_celu";
    }

    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co chcete zničit?";
        } else {
            String lektvar = parametry[0];
            Batoh kosik = this.plan.getKosik();
            if (kosik.vKosiku("Lektvar_sily")) {
                Collection<Prostor> okoli = this.plan.getAktualniProstor().getVychody();
                Iterator var5 = okoli.iterator();

                while(var5.hasNext()) {
                    Prostor i = (Prostor)var5.next();
                    if (i.jeZnicitelna()) {
                        i.setZnicitelna(false);
                        this.plan.setVyhraHonza(true);
                    }
                }

                return "Zničili jste mříže cely\nHonza: děkuji ti za záchranu. Teď už na nic nečekej a utíkej. Doženu tě později!";
            } else {
                return "Nemáš lektvar síly, nebo se snažíš probourat bránu / celu něčím jiným!";
            }
        }
    }
}