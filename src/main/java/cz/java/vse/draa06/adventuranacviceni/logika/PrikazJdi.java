package cz.java.vse.draa06.adventuranacviceni.logika;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */
public class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    private Hra hra;

    public PrikazJdi(HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.hra = hra;
    }

    public PrikazJdi(HerniPlan herniPlan) {

    }

    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Kam mám jít? Musíš zadat jméno východu";
        } else {
            String smer = parametry[0];
            Prostor sousedniProstor = this.plan.getAktualniProstor().vratSousedniProstor(smer);
            if (sousedniProstor != null && sousedniProstor.jeViditelna()) {
                if (sousedniProstor.jeSmrt()) {
                    this.hra.setKonecHry(true);
                    return "Umřel jsi!";
                } else if (sousedniProstor.jeZamceno()) {
                    return "Je zamčeno! Potřebuješ klíč.";
                } else if (sousedniProstor.jeZnicitelna()) {
                    return "Bránu neotevřeš jen tak...";
                } else {
                    this.plan.setAktualniProstor(sousedniProstor);
                    return sousedniProstor.dlouhyPopis();
                }
            } else {
                return "Tam se odsud jít nedá!";
            }
        }
    }

    public String getNazev() {
        return "jdi";
    }
}